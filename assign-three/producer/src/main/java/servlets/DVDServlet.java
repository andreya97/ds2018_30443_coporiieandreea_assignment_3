package servlets;

import ds.ProducerService;
import entity.DVD;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class DVDServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher view = req.getRequestDispatcher("DVD.html");
        view.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ProducerService ps = new ProducerService();

        try {
            String title = req.getParameter("title");
            Integer year = Integer.parseInt(req.getParameter("year"));
            Double price = Double.parseDouble(req.getParameter("price"));

            ps.publishDvd(new DVD(title, year, price));
        } catch (Exception e) {
            System.out.println("Something went wrong: " + e.getMessage());
        }

        RequestDispatcher view = req.getRequestDispatcher("DVD.html");
        view.forward(req,resp);
    }
}
