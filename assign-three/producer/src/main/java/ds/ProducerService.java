package ds;

import com.google.gson.Gson;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import entity.DVD;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class ProducerService  {

    private final static String QUEUE_NAME = "DVD queue";

    public void publishDvd(DVD dvd) throws IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        Gson gson = new Gson();
        String message = gson.toJson(dvd);

        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
        channel.basicPublish("", QUEUE_NAME, null, message.getBytes());

        System.out.println("Sent message: " + message);

        channel.close();
        connection.close();
    }
}
