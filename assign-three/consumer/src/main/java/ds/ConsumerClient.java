package ds;

import com.google.gson.Gson;
import com.rabbitmq.client.*;
import entity.DVD;
import service.MailService;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeoutException;

import static asd.Secret.PASSWORD;
import static asd.Secret.USERNAME;

public class ConsumerClient {
    private static final String QUEUE_NAME = "DVD queue";

    public static void main (String[] args) throws IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.queueDeclare(QUEUE_NAME, false, false, false, null);


        final List<String> subscribers = new ArrayList<String>();
        subscribers.add("coporiie.andreea@gmail.com");
        subscribers.add("ardeleaneugenrichard@gmail.com");

        Consumer consumer = new DefaultConsumer(channel) {
            MailService mailService = new MailService(USERNAME,PASSWORD);
            Gson gson = new Gson();
            int i = 1;

            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                String message = new String(body, "UTF-8");
                DVD dvd = gson.fromJson(message, DVD.class);

                //Notify subscribers
                for (String subscriber : subscribers) {
                    mailService.sendMail(subscriber,"DS Notif ",message);
                }

                //Write each message into a new file
                PrintWriter writer = new PrintWriter(new File("consumedMessage" + i++ + ".txt"));
                writer.println(dvd);
                writer.flush();
                writer.close();

                System.out.println("Received: " + dvd);
            }
        };
        channel.basicConsume(QUEUE_NAME, true, consumer);
    }
}
